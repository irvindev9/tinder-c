<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',function(){return view('welcome');});
Route::get('/interesado',function(){
    if(isset($_GET['mat'])){
        return redirect('/?success');
    }
}); 
/*Route::get('/', function () {
    if(session()->has('id')){
        //Nuevo registro
    }else{
        //Pagina de inicio
    }
    return view('registro.inicio');
});*/

//Resto del registro
Route::get('registro/{matricula}/{llave}',function($matricula,$llave){
    //Verificar si existe la matricula y mandar a la vista la matricula para completar el registro
    if(true){
        return view('registro.adicional',['matricula' => $matricula,'llave' => $llave]);
    }else{
        return redirect('/');
    }
});
