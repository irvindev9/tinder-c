@extends('index')
@section('contenido')

<div class="container">
    <div class="row">
        <div class="col-12">
            <label class="sub-titulo" for="Sub">Completa el registro para continuar</label>
        </div>
        <div class="col-12">
            <form>
                <input type="hidden" name="llave" value="{{$llave}}">
                <div class="form-group">
                  <label for="matricula">Matricula</label>
                  <input disabled type="text" class="form-control" id="matricula" placeholder="" value="{{$matricula}}">
                  <!--small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small-->
                </div>
                <div class="form-group">
                  <label for="Nombre">Nombre completo</label>
                  <input type="text" class="form-control" id="Nombre" placeholder="Ingresa tu nombre completo">
                </div>
                <!-- div class="form-group form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div-->
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Edad</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                        <option>18</option>
                        <option>19</option>
                        <option>20</option>
                        <option>21</option>
                        <option>22</option>
                        <option>23</option>
                        <option>24</option>
                        <option>25</option>
                        <option>26</option>
                        <option>27</option>
                        <option>28</option>
                        <option>29</option>
                    </select>
                </div>    
                <div class="form-group">
                        <label for="exampleFormControlSelect1">Carrera</label>
                        <select class="form-control" id="exampleFormControlSelect1">
                            <option value="default">Selecciona</option>
                            <optgroup label="CU">
                                <option value="CU - Publicidad">Publicidad</option>
                                <option value="CU - Sistemas Automotrices">Sistemas Automotrices</option>
                                <option value="CU - Enseñanza del ingles">Enseñanza del ingles</option>
                                <option value="CU - Comercio Exterior">Comercio Exterior</option>
                                <option value="CU - Periodismo">Periodismo</option>
                                <option value="CU - Gerontologia">Gerontologia</option>
                                <option value="CU - Software">Software</option>
                                <option value="CU - Arquitectura">Arquitectura</option>
                                <option value="CU - Diseño Grafico">Diseño Grafico</option>
                                <option value="CU - Medico Veterinario Zootecnista">Medico Veterinario Zootecnista</option>
                                <option value="CU - Nutricion">Nutricion</option>
                                <option value="CU - Entrenamiento Deportivo">Entrenamiento Deportivo</option>
                                <option value="CU - Enfermeria">Enfermeria</option>
                                <option value="CU - Industrial y de Sistemas">Industrial y de Sistemas</option>
                                <option value="CU - Mecatronica">Mecatronica</option>
                                <option value="CU - Sistemas Computacionales">Sistemas Computacionales</option>
                                <option value="CU - Derecho">Derecho</option>
                                <option value="CU - Administracion de Empresas">Administracion de Empresas</option>
                                <option value="CU - Trabajo Social">Trabajo Social</option>
                                <option value="CU - Psicologia">Psicologia</option>
                                <option value="CU - Contaduria">Contaduria</option>
                                <option value="CU - Educacion">Educacion</option>
                            </optgroup>
                        </select>
                    </div>
                    <label for="Soy">Soy ...</label>
                    <div class="form-group form-check">
                        <input type="radio" name="gender" class="form-check-input" id="men">
                        <label class="form-check-label" for="men">Hombre</label>
                    </div>  
                    <div class="form-group form-check">
                        <input type="radio" name="gender" class="form-check-input" id="woman">
                        <label class="form-check-label" for="woman">Mujer</label>
                    </div>       
                <button type="submit" class="btn btn-primary btn-block boton_enviar">Registrarme</button>
              </form>
        </div>
    </div>
</div>

@stop