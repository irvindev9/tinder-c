@extends('index')
@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-12 col-md-6 offset-md-3">
            <center><img src="https://img.icons8.com/color/96/000000/novel.png"></center>
            <center><label class="titulo" for="titulo">tinderCU</label></center>
            <br>
        </div>
        <div class="col-12 col-md-6 offset-md-3">
            <label for="anuncio">Sitio exclusivo para alumnos de la GUACEJOTA, para continuar ingresa tu
                matricula.<br><a class="bold">NOTA:</a> Se enviara un enlace a tu correo para continuar con el
                registro.</label>
            <br>
        </div>
        <div class="col-12">
            <form>
                <div class="form-row align-items-center formulario">
                    <div class="col-12 col-md-4 offset-md-4">
                        <label class="sr-only" for="inlineFormInputGroup">Matricula</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">al</div>
                            </div>
                            <input type="text" class="form-control" id="inlineFormInputGroup" placeholder="Matricula">
                            <div class="input-group-prepend">
                                <div class="input-group-text">@alumnos.uacj.mx</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 offset-md-4">
                        <center><button type="submit" class="btn btn-primary btn-block mb-2 boton_enviar">Enviar</button></center>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@stop