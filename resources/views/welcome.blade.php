<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>TinderCU</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/estilos_generales.css">
    <link href='https://fonts.googleapis.com/css?family=Allan' rel='stylesheet'>
    <link rel="icon" type="image/png" href="https://img.icons8.com/color/48/000000/novel.png" />

</head>

<body>
    <header>
        @include('extras.header')
    </header>

    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6 offset-md-3">
                <center><img src="https://img.icons8.com/color/96/000000/novel.png"></center>
                <center><label class="titulo" for="titulo">tinderCU</label></center>
                <br>
            </div>
            <div class="col-12 col-md-6 offset-md-3">
                <center><label for="anuncio" class="bold">Proximamente</label></center>
                <br>
                <center><div class="bold" style="text-align:center;" id="countdown"></div></center>
                <br>
                <center><img src="https://img.icons8.com/office/80/000000/in-love.png"></center>
            </div>
            @if(!isset($_GET['success']))
            <div class="col-12">
                    <center><label for="anuncio" class="bold">Si te interesa el proyecto ingresa tu matricula para apoyar!</label></center>
                <form actio="/interesado" method="GET">
                    <div class="form-row align-items-center formulario">
                        <div class="col-12 col-md-4 offset-md-4">
                            <label class="sr-only" for="inlineFormInputGroup">Matricula</label>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">al</div>
                                </div>
                                <input type="text" name="mat" class="form-control" id="inlineFormInputGroup" placeholder="Matricula">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">@alumnos.uacj.mx</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 offset-md-4">
                            <center><button type="submit" class="btn btn-primary btn-block mb-2 boton_enviar">Enviar</button></center>
                        </div>
                    </div>
                </form>
            </div>
            @else 
            <div class="col-12">
                    <center><label style="font-size:20px;font-weight:bold;color:#B71D1D;" for="thanks">Gracias!</label></center>
            </div>
            @endif
            <script>
            var end = new Date('04/21/2019 1:00 AM');

                var _second = 1000;
                var _minute = _second * 60;
                var _hour = _minute * 60;
                var _day = _hour * 24;
                var timer;

                function showRemaining() {
                    var now = new Date();
                    var distance = end - now;
                    if (distance < 0) {

                        clearInterval(timer);
                        document.getElementById('countdown').innerHTML = 'EXPIRED!';

                        return;
                    }
                    var days = Math.floor(distance / _day);
                    var hours = Math.floor((distance % _day) / _hour);
                    var minutes = Math.floor((distance % _hour) / _minute);
                    var seconds = Math.floor((distance % _minute) / _second);

                    document.getElementById('countdown').innerHTML = days + ' dias, ';
                    document.getElementById('countdown').innerHTML += hours + ' horas, ';
                    document.getElementById('countdown').innerHTML += minutes + ' minutos y ';
                    document.getElementById('countdown').innerHTML += seconds + ' segundos';
                }

                timer = setInterval(showRemaining, 1000);
            </script>
        </div>
    </div>
    

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
</body>

</html>